package ru.t1.schetinin.tm.api.repository;

import ru.t1.schetinin.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectRepository extends IRepository<Project> {

    Project create(String name, String description);

    Project create(String name);

}