package ru.t1.schetinin.tm.api.repository;

import ru.t1.schetinin.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskRepository extends IRepository<Task> {

    List<Task> findAllByProjectId(String projectId);

    Task create(String name, String description);

    Task create(String name);

}
