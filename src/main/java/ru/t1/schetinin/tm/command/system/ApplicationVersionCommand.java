package ru.t1.schetinin.tm.command.system;

public final class ApplicationVersionCommand extends AbstractSystemCommand{

    public static final String NAME = "version";

    public static final String DESCRIPTION = "Show version info.";

    public static final String ARGUMENT = "-v";

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        System.out.println("1.18.0");
    }

}